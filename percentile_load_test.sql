DROP EXTENSION IF EXISTS histogram cascade;
CREATE EXTENSION histogram;


DROP TABLE IF EXISTS foo;
CREATE TABLE foo (
    bar real
);

INSERT INTO foo (bar)
SELECT
    (random() * 1000)::decimal(10,2)::real - 500
FROM
    generate_series(1, 10000);


DROP TABLE IF EXISTS foo_histograms;
CREATE TABLE foo_histograms (
    bar real_histogram
);

INSERT INTO foo_histograms (bar)
SELECT
    agg_real_to_histogram() WITHIN GROUP (ORDER BY bar)
FROM
    foo;

SELECT percentile_cont(.75) WITHIN GROUP (ORDER BY bar) FROM foo;
SELECT histogram_percentile_cont(bar, .75) FROM foo_histograms;

SELECT percentile_disc(.75) WITHIN GROUP (ORDER BY bar) FROM foo;
SELECT histogram_percentile_disc(bar, .75) FROM foo_histograms;

SELECT mode() WITHIN GROUP (ORDER BY bar) FROM foo;
SELECT mode(bar) FROM foo_histograms;

SELECT min(bar) FROM foo;
SELECT min(bar) FROM foo_histograms;

SELECT max(bar) FROM foo;
SELECT max(bar) FROM foo_histograms;

SELECT sum(bar) FROM foo;
SELECT sum(bar) FROM foo_histograms;

SELECT avg(bar) FROM foo;
SELECT avg(bar) FROM foo_histograms;

SELECT count(bar) FROM foo;
SELECT count(bar) FROM foo_histograms;

SELECT percentile_disc(ARRAY[.25, .5, .75]) WITHIN GROUP (ORDER BY bar) FROM foo;
SELECT histogram_percentile_disc(bar, ARRAY[.25, .5, .75]) FROM foo_histograms;

SELECT percentile_cont(ARRAY[.25, .5, .75]) WITHIN GROUP (ORDER BY bar) FROM foo;
SELECT histogram_percentile_cont(bar, ARRAY[.25, .5, .75]) FROM foo_histograms;

SELECT bar FROM foo ORDER BY bar;
SELECT unnest(bar) FROM foo_histograms;
