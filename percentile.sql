DROP EXTENSION IF EXISTS histogram CASCADE;
CREATE EXTENSION histogram;

DROP TABLE IF EXISTS foo;
CREATE TABLE foo (
    bar real
);

INSERT INTO foo (bar) VALUES
    (1), (2.25), (2), (3), (3), (3.3), (3.4), (4), (5), (5), (5), (5), (5), (6), (6), (6), (6), (6), (6), (6), (6), (7), (8), (8), (8), (8), (8), (8), (9), (9), (9), (10), (10), (10), (10), (11), (11), (11);

DROP TABLE IF EXISTS foo_histograms;
CREATE TABLE foo_histograms (
    bar real_histogram
);

INSERT INTO foo_histograms (bar)
SELECT
    agg_real_to_histogram() WITHIN GROUP (ORDER BY bar)
FROM
    foo;

SELECT histogram_percentile_cont(bar, .75) FROM foo_histograms;
SELECT percentile_cont(.75) WITHIN GROUP (ORDER BY bar) FROM foo;
-- select * from foo limit 4;
-- select * from foo_histograms;

-- WITH gs AS (
--     select * from generate_series(0, 1, .01) AS t
-- )
-- select
--     gs.t AS percentile,
--     (SELECT histogram_percentile_disc(bar, gs.t) FROM foo_histograms) AS histogram_percentile_disc,
--     (SELECT percentile_disc(gs.t) WITHIN GROUP (ORDER BY bar) FROM foo) AS percentile_disc,
--     (SELECT histogram_percentile_cont(bar, gs.t) FROM foo_histograms) AS histogram_percentile_cont,
--     (SELECT percentile_cont(gs.t) WITHIN GROUP (ORDER BY bar) FROM foo) AS percentile_cont
-- from
--     gs;

-- SELECT percentile_disc(ARRAY[.25, .5, .75]) WITHIN GROUP (ORDER BY bar) FROM foo;
-- SELECT histogram_percentile_disc(bar, ARRAY[.25, .5, .75]) FROM foo_histograms;

SELECT * FROM foo;
SELECT unnest(bar) AS bar FROM foo_histograms;

-- SELECT percentile_cont(ARRAY[.25, .5, .75]) WITHIN GROUP (ORDER BY bar) FROM foo;
-- SELECT histogram_percentile_cont(bar, ARRAY[.25, .5, .75]) FROM foo_histograms;

