MODULES = histogram
EXTENSION = histogram
DATA = histogram--1.0.sql
REGRESS = histogram_test

OBJS = $(patsubst %.c,%.o,$(wildcard src/*.c)) # object files

# final shared library to be build from multiple source files (OBJS)
MODULE_big = $(EXTENSION)

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
