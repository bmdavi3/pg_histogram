-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION histogram" to load this file. \quit


-- real histogram
CREATE TYPE real_histogram AS (
  distinct_values int,
  value_sum double precision,
  mode real,
  values real[],
  running_occurrence_totals bigint[]
);

CREATE FUNCTION agg_real_to_histogram_final(internal)
RETURNS real_histogram
AS 'MODULE_PATHNAME', 'agg_real_to_histogram_final'
LANGUAGE C STRICT IMMUTABLE;

CREATE AGGREGATE agg_real_to_histogram(ORDER BY real) (
  stype = internal,
  sfunc = ordered_set_transition,
  finalfunc = agg_real_to_histogram_final,
  finalfunc_modify = read_write
);

CREATE FUNCTION histogram_agg(real_histogram, real_histogram)
RETURNS real_histogram
AS 'MODULE_PATHNAME', 'real_histogram_agg'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE AGGREGATE histogram_agg (real_histogram)
(
    sfunc = histogram_agg,
    stype = real_histogram
);

CREATE FUNCTION histogram_percentile_disc(real_histogram, real[])
RETURNS real[]
AS 'MODULE_PATHNAME', 'real_histogram_percentile_disc_array'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_disc(histogram real_histogram, percentile real) RETURNS real AS $$
  SELECT (histogram_percentile_disc(histogram, ARRAY[percentile]))[1];
$$ LANGUAGE SQL STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_cont(real_histogram, real[])
RETURNS real[]
AS 'MODULE_PATHNAME', 'real_histogram_percentile_cont_array'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_cont(histogram real_histogram, percentile real) RETURNS real AS $$
  SELECT (histogram_percentile_cont(histogram, ARRAY[percentile]))[1];
$$ LANGUAGE SQL STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION sum(real_histogram)
RETURNS double precision
AS 'MODULE_PATHNAME', 'real_histogram_sum'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION avg(real_histogram)
RETURNS double precision
AS 'MODULE_PATHNAME', 'real_histogram_avg'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION mode(real_histogram)
RETURNS real
AS 'MODULE_PATHNAME', 'real_histogram_mode'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION min(real_histogram)
RETURNS real
AS 'MODULE_PATHNAME', 'real_histogram_min'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION max(real_histogram)
RETURNS real
AS 'MODULE_PATHNAME', 'real_histogram_max'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION count(real_histogram)
RETURNS bigint
AS 'MODULE_PATHNAME', 'real_histogram_count'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION unnest(real_histogram)
RETURNS setof real
AS 'MODULE_PATHNAME', 'real_histogram_unnest'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;


-- Numeric Histogram
CREATE TYPE numeric_histogram AS (
  distinct_values int,
  value_sum numeric,
  mode numeric,
  values numeric[],
  running_occurrence_totals bigint[]
);

CREATE FUNCTION agg_numeric_to_histogram_final(internal)
RETURNS numeric_histogram
AS 'MODULE_PATHNAME', 'agg_numeric_to_histogram_final'
LANGUAGE C STRICT IMMUTABLE;

CREATE AGGREGATE agg_numeric_to_histogram(ORDER BY numeric) (
  stype = internal,
  sfunc = ordered_set_transition,
  finalfunc = agg_numeric_to_histogram_final,
  finalfunc_modify = read_write
);

CREATE FUNCTION histogram_agg(numeric_histogram, numeric_histogram)
RETURNS numeric_histogram
AS 'MODULE_PATHNAME', 'numeric_histogram_agg'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE AGGREGATE histogram_agg (numeric_histogram)
(
    sfunc = histogram_agg,
    stype = numeric_histogram
);

CREATE FUNCTION unnest(numeric_histogram)
RETURNS setof numeric
AS 'MODULE_PATHNAME', 'numeric_histogram_unnest'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_disc(numeric_histogram, real[])
RETURNS numeric[]
AS 'MODULE_PATHNAME', 'numeric_histogram_percentile_disc_array'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_disc(histogram numeric_histogram, percentile real) RETURNS numeric AS $$
  SELECT (histogram_percentile_disc(histogram, ARRAY[percentile]))[1];
$$ LANGUAGE SQL STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_cont(numeric_histogram, real[])
RETURNS numeric[]
AS 'MODULE_PATHNAME', 'numeric_histogram_percentile_cont_array'
LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION histogram_percentile_cont(histogram numeric_histogram, percentile real) RETURNS numeric AS $$
  SELECT (histogram_percentile_cont(histogram, ARRAY[percentile]))[1];
$$ LANGUAGE SQL STRICT IMMUTABLE PARALLEL SAFE;
