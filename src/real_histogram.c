#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "postgres.h"
#include <catalog/pg_type.h>
#include <utils/array.h>
#include "access/htup_details.h"
#include "executor/executor.h"
#include "funcapi.h"
#include "utils/builtins.h"
#include "utils/fmgrprotos.h"
#include "utils/lsyscache.h"

#include "histogram.h"

/*
 * TODO:
 *   - Do the same refactoring for Numeric histograms
 *   - Allow subtraction of one histogram from another
 *   - Allow subtraction of a real from a histogram
 *   - Support Numeric data type
 *   - Contains functionality / gin / gist?
 *   - Compare null handling in aggs to Postgres' built in functions
 */

PG_MODULE_MAGIC;


float4 *unpack_values_from_real_histogram(HeapTupleHeader t);
float4 *unpack_values_from_real_histogram(HeapTupleHeader t) {
  ArrayType *values_array_type;
  bool is_null;

  values_array_type = DatumGetArrayTypeP(GetAttributeByName(t, "values", &is_null));

  return (float4 *)ARR_DATA_PTR(values_array_type);
}


float8 unpack_value_sum_from_real_histogram(HeapTupleHeader t);
float8 unpack_value_sum_from_real_histogram(HeapTupleHeader t) {
  bool is_null;

  return DatumGetFloat8(GetAttributeByName(t, "value_sum", &is_null));
}


float4 unpack_mode_from_real_histogram(HeapTupleHeader t);
float4 unpack_mode_from_real_histogram(HeapTupleHeader t) {
  bool is_null;

  return DatumGetFloat4(GetAttributeByName(t, "mode", &is_null));
}


void unpack_real_histogram(HeapTupleHeader t, int64 **running_occurrence_totals, float4 **values, int *distinct_values);
void unpack_real_histogram(HeapTupleHeader t, int64 **running_occurrence_totals, float4 **values, int *distinct_values) {

  *distinct_values = unpack_distinct_values_from_histogram(t);
  *running_occurrence_totals = unpack_running_occurrence_totals_from_histogram(t);
  *values = unpack_values_from_real_histogram(t);
}


typedef struct RealUnnestContext {
  int64 *running_occurrence_totals;
  float4 *values;
  int pairs_idx;
} RealUnnestContext;


PG_FUNCTION_INFO_V1(real_histogram_unnest);
Datum real_histogram_unnest(PG_FUNCTION_ARGS)
{
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  FuncCallContext     *funcctx;
  int                  call_cntr;
  int                  max_calls;
  int distinct_values;
  int64 *running_occurrence_totals;
  float4 *values;
  RealUnnestContext *unnest_context;

  /* stuff done only on the first call of the function */
  if (SRF_IS_FIRSTCALL()) {
    MemoryContext   oldcontext;

    /* create a function context for cross-call persistence */
    funcctx = SRF_FIRSTCALL_INIT();

    /* switch to memory context appropriate for multiple function calls */
    oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

    funcctx->user_fctx = (RealUnnestContext *)palloc0(sizeof(RealUnnestContext));

    unnest_context = (RealUnnestContext *)funcctx->user_fctx;

    unpack_real_histogram(t, &running_occurrence_totals, &values, &distinct_values);

    /* Stick them on unnest_context */
    unnest_context->running_occurrence_totals = running_occurrence_totals;
    unnest_context->values = values;

    unnest_context->pairs_idx = 0;

    funcctx->max_calls = running_occurrence_totals[distinct_values - 1];  // Total number of tuples to be returned

    MemoryContextSwitchTo(oldcontext);
  }

  /* stuff done on every call of the function */
  funcctx = SRF_PERCALL_SETUP();

  call_cntr = funcctx->call_cntr;
  max_calls = funcctx->max_calls;

  unnest_context = (RealUnnestContext *)funcctx->user_fctx;
  running_occurrence_totals = unnest_context->running_occurrence_totals;
  values = unnest_context->values;

  if (call_cntr < max_calls) {
    if (call_cntr >= running_occurrence_totals[unnest_context->pairs_idx]) {
      unnest_context->pairs_idx++;
    }
    SRF_RETURN_NEXT(funcctx, Float4GetDatum(values[unnest_context->pairs_idx]));
  }
  else {
    SRF_RETURN_DONE(funcctx);
  }
}


void set_occurrences(Datum *running_occurrence_totals, int index, int occurrences) {
  if (index == 0) {
    running_occurrence_totals[0] = Int64GetDatum(occurrences);
  } else {
    running_occurrence_totals[index] = Int64GetDatum(DatumGetInt64(running_occurrence_totals[index - 1]) + occurrences);
  }
}


int get_occurrences(int64 *running_occurrence_totals, int index) {
  if (index == 0) {
    return running_occurrence_totals[0];
  }
  return running_occurrence_totals[index] - running_occurrence_totals[index - 1];
}


int get_occurrences_from_datums(Datum *running_occurrence_totals, int index) {
  if (index == 0) {
    return DatumGetInt64(running_occurrence_totals[0]);
  }
  return DatumGetInt64(running_occurrence_totals[index]) - DatumGetInt64(running_occurrence_totals[index - 1]);
}


PG_FUNCTION_INFO_V1(agg_real_to_histogram_final);
Datum agg_real_to_histogram_final(PG_FUNCTION_ARGS)
{
  OSAPerGroupState *osastate;
  Datum	val;
  bool isnull;
  float4 previous_value = 0;
  int pairs_index = 0;
  int occurrences = 0;
  int max_occurrences = 0;
  int i = 0;
  int num_sorted_values = 0;
  float4 *sorted_values;
  int distinct_values = 0;
  float8 value_sum = 0;
  float4 mode = 0;
  TupleDesc tupdesc;
  Datum result[5];
  bool result_isnull[5];
  HeapTuple tuple;
  Datum *values;
  Datum *running_occurrence_totals;

  /* Get the tupdesc for our call to heap_form_tuple() at the end.
   * Postgres knows the return type of our function, so this is the easiest way to get it */
  get_call_result_type(fcinfo, NULL, &tupdesc);
  BlessTupleDesc(tupdesc);

  Assert(AggCheckCallContext(fcinfo, NULL) == AGG_CONTEXT_AGGREGATE);

  /* If there were no regular rows, the result is NULL */
  if (PG_ARGISNULL(0)) {
    PG_RETURN_NULL();
  }

  osastate = (OSAPerGroupState *) PG_GETARG_POINTER(0);

  /* number_of_rows could be zero if we only saw NULL input values */
  if (osastate->number_of_rows == 0) {
    PG_RETURN_NULL();
  }

  /* Finish the sort, or rescan if we already did */
  if (!osastate->sort_done) {
    tuplesort_performsort(osastate->sortstate);
    osastate->sort_done = true;
  }
  else {
    tuplesort_rescan(osastate->sortstate);
  }
  num_sorted_values = osastate->number_of_rows;

  sorted_values = (float4*)palloc0(sizeof(float4) * num_sorted_values);

  /* Get the number of distinct values, so we know how big our histogram will be */
  if (tuplesort_getdatum(osastate->sortstate, true, &val, &isnull, NULL)) {
    distinct_values++;
    sorted_values[i] = DatumGetFloat4(val);
    previous_value = sorted_values[i];
    i++;
  }

  while(tuplesort_getdatum(osastate->sortstate, true, &val, &isnull, NULL)) {
    if (previous_value != DatumGetFloat4(val)) {
      distinct_values++;
      previous_value = DatumGetFloat4(val);
    }
    sorted_values[i] = DatumGetFloat4(val);
    i++;
  }

  /* Now that we can palloc0 our values and running_occurrence_totals */
  values = (Datum*)palloc0(sizeof(Datum) * distinct_values);
  running_occurrence_totals = (Datum*)palloc0(sizeof(Datum) * distinct_values);

  i = 0;

  /* Loop through our sorted values, and record values and running_occurrence_totals */
  if (num_sorted_values > 0) {
    values[pairs_index] = Float4GetDatum(sorted_values[i]);
    running_occurrence_totals[pairs_index] = Int64GetDatum(1);
    previous_value = sorted_values[i];
  }

  while(i + 1 < num_sorted_values) {
    i++;
    if (previous_value == sorted_values[i]) {
      running_occurrence_totals[pairs_index] = Int64GetDatum(DatumGetInt64(running_occurrence_totals[pairs_index]) + 1);
    } else {
      pairs_index++;
      values[pairs_index] = Float4GetDatum(sorted_values[i]);
      running_occurrence_totals[pairs_index] = Int64GetDatum(DatumGetInt64(running_occurrence_totals[pairs_index - 1]) + 1);
      previous_value = sorted_values[i];
    }
  }

  // Compute value_sum, mode
  for (i = 0; i < distinct_values; i++) {
    if (i == 0) {
      occurrences = DatumGetInt64(running_occurrence_totals[i]);
    } else {
      occurrences = DatumGetInt64(running_occurrence_totals[i]) - DatumGetInt64(running_occurrence_totals[i - 1]);
    }

    value_sum += DatumGetFloat4(values[i]) * occurrences;

    if (occurrences > max_occurrences) {
      mode = DatumGetFloat4(values[i]);
      max_occurrences = occurrences;
    }
  }

  result[0] = Int32GetDatum(distinct_values);
  result[1] = Float8GetDatum(value_sum);
  result[2] = Float4GetDatum(mode);
  result[3] = PointerGetDatum(construct_array(values, distinct_values, FLOAT4OID, sizeof(float4), true, 'z'));
  result[4] = PointerGetDatum(construct_array(running_occurrence_totals, distinct_values, INT8OID, sizeof(int64), true, 'i'));

  result_isnull[0] = false;
  result_isnull[1] = false;
  result_isnull[2] = false;
  result_isnull[3] = false;
  result_isnull[4] = false;

  /* Form and return the tuple. */
  tuple = heap_form_tuple(tupdesc, result, result_isnull);
  PG_RETURN_DATUM(HeapTupleGetDatum(tuple));
}


PG_FUNCTION_INFO_V1(float4_sub);
Datum float4_sub(PG_FUNCTION_ARGS)
{
	float4 arg1 = PG_GETARG_FLOAT4(0);
	float4 arg2 = PG_GETARG_FLOAT4(1);

	PG_RETURN_FLOAT4(arg1 - arg2);
}


PG_FUNCTION_INFO_V1(float4_add);
Datum float4_add(PG_FUNCTION_ARGS)
{
	float4 arg1 = PG_GETARG_FLOAT4(0);
	float4 arg2 = PG_GETARG_FLOAT4(1);

	PG_RETURN_FLOAT4(arg1 + arg2);
}


PG_FUNCTION_INFO_V1(float4_float4);
Datum float4_float4(PG_FUNCTION_ARGS)
{
	float4 arg1 = PG_GETARG_FLOAT4(0);

	PG_RETURN_FLOAT4(arg1);
}


PG_FUNCTION_INFO_V1(real_histogram_percentile_disc_array);
Datum real_histogram_percentile_disc_array(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  ArrayType *percentiles_array = PG_GETARG_ARRAYTYPE_P(1);
  ArrayType *results_array;

  results_array = generic_histogram_percentile_array(t, percentiles_array, false, float4_sub, float4_float4, float4mul, float4_add);

  PG_RETURN_ARRAYTYPE_P(results_array);
}


PG_FUNCTION_INFO_V1(real_histogram_percentile_cont_array);
Datum real_histogram_percentile_cont_array(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  ArrayType *percentiles_array = PG_GETARG_ARRAYTYPE_P(1);
  ArrayType *results_array;

  results_array = generic_histogram_percentile_array(t, percentiles_array, true, float4_sub, float4_float4, float4mul, float4_add);

  PG_RETURN_ARRAYTYPE_P(results_array);
}


PG_FUNCTION_INFO_V1(real_histogram_min);
Datum real_histogram_min(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);

  PG_RETURN_FLOAT4(unpack_values_from_real_histogram(t)[0]);
}


PG_FUNCTION_INFO_V1(real_histogram_max);
Datum real_histogram_max(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int distinct_values;
  float4 *values;

  distinct_values = unpack_distinct_values_from_histogram(t);
  values = unpack_values_from_real_histogram(t);

  PG_RETURN_FLOAT4(values[distinct_values - 1]);
}


PG_FUNCTION_INFO_V1(real_histogram_sum);
Datum real_histogram_sum(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);

  PG_RETURN_FLOAT8(unpack_value_sum_from_real_histogram(t));
}


PG_FUNCTION_INFO_V1(real_histogram_count);
Datum real_histogram_count(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int distinct_values;
  int64 *running_occurrence_totals;

  distinct_values = unpack_distinct_values_from_histogram(t);
  running_occurrence_totals = unpack_running_occurrence_totals_from_histogram(t);

  PG_RETURN_INT64(running_occurrence_totals[distinct_values - 1]);
}


PG_FUNCTION_INFO_V1(real_histogram_avg);
Datum real_histogram_avg(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  float8 value_sum;
  int distinct_values;
  int64 *running_occurrence_totals;

  value_sum = unpack_value_sum_from_real_histogram(t);
  distinct_values = unpack_distinct_values_from_histogram(t);
  running_occurrence_totals = unpack_running_occurrence_totals_from_histogram(t);

  PG_RETURN_FLOAT8(value_sum / running_occurrence_totals[distinct_values - 1]);
}


PG_FUNCTION_INFO_V1(real_histogram_mode);
Datum real_histogram_mode(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);

  PG_RETURN_FLOAT4(unpack_mode_from_real_histogram(t));
}


PG_FUNCTION_INFO_V1(real_histogram_agg);
Datum real_histogram_agg(PG_FUNCTION_ARGS) {
  HeapTupleHeader t1 = PG_GETARG_HEAPTUPLEHEADER(0);
  HeapTupleHeader t2 = PG_GETARG_HEAPTUPLEHEADER(1);
  int distinct_values_1;
  int distinct_values_2;
  int64 *running_occurrence_totals_1;
  int64 *running_occurrence_totals_2;
  float4 *values_1;
  float4 *values_2;

  Datum *combined_values;
  Datum *combined_running_occurrence_totals;
  int combined_distinct_values = 0;
  int max_occurrences = 0;
  int occurrences = 0;
  float4 mode = 0;
  int h1 = 0;
  int h2 = 0;
  Datum result[5];
  bool result_isnull[5];
  HeapTuple tuple;
  TupleDesc tupdesc;

  /* Get the tupdesc for our call to heap_form_tuple() at the end.
   * Postgres knows the return type of our function, so this is the easiest way to get it */
  get_call_result_type(fcinfo, NULL, &tupdesc);
  BlessTupleDesc(tupdesc);

  unpack_real_histogram(t1, &running_occurrence_totals_1, &values_1, &distinct_values_1);
  unpack_real_histogram(t2, &running_occurrence_totals_2, &values_2, &distinct_values_2);

  while (h1 < distinct_values_1 || h2 < distinct_values_2) {

    if (h1 < distinct_values_1 && // Case 1: h1 has values and...
	(h2 == distinct_values_2 || // h2 doesn't OR
	 values_1[h1] < values_2[h2])) { // h1 < h2
      h1++;
    } else if (h2 < distinct_values_2 && // Case 2: h2 has values and...
	       (h1 == distinct_values_1 || // h1 doesn't OR
		values_1[h1] > values_2[h2])) { // h2 < h1
      h2++;
    } else if (values_1[h1] == values_2[h2]) {
      // Case 3: Both have values, h1 == h2.  Add together, increment both
      h1++;
      h2++;
    } else {
      // Panic!
      ereport(ERROR, (errcode(ERRCODE_INTERNAL_ERROR), errmsg("Should never get here")));
    }
    combined_distinct_values++;
  }

  /* Now that we can palloc0 our values and running_occurrence_totals */
  combined_values = (Datum*)palloc0(sizeof(Datum) * combined_distinct_values);
  combined_running_occurrence_totals = (Datum*)palloc0(sizeof(Datum) * combined_distinct_values);

  h1 = 0;
  h2 = 0;
  combined_distinct_values = 0;

  while (h1 < distinct_values_1 || h2 < distinct_values_2) {
    if (h1 < distinct_values_1 && // Case 1: h1 has values and...
	(h2 == distinct_values_2 || // h2 doesn't OR
	 values_1[h1] < values_2[h2])) { // h1 < h2
      combined_values[combined_distinct_values] = Float4GetDatum(values_1[h1]);
      set_occurrences(combined_running_occurrence_totals, combined_distinct_values, get_occurrences(running_occurrence_totals_1, h1));
      h1++;

    } else if (h2 < distinct_values_2 && // Case 2: h2 has values and...
	       (h1 == distinct_values_1 || // h1 doesn't OR
		values_1[h1] > values_2[h2])) { // h2 < h1
      combined_values[combined_distinct_values] = Float4GetDatum(values_2[h2]);
      set_occurrences(combined_running_occurrence_totals, combined_distinct_values, get_occurrences(running_occurrence_totals_2, h2));
      h2++;
    } else if (values_1[h1] == values_2[h2]) {
      // Case 3: Both have values, h1 == h2.  Add together, increment both
      combined_values[combined_distinct_values] = Float4GetDatum(values_1[h1]);
      set_occurrences(combined_running_occurrence_totals, combined_distinct_values, get_occurrences(running_occurrence_totals_1, h1) + get_occurrences(running_occurrence_totals_2, h2));
      h1++;
      h2++;
    } else {
      // Panic!
      ereport(ERROR, (errcode(ERRCODE_INTERNAL_ERROR), errmsg("Should never get here")));
    }

    occurrences = get_occurrences_from_datums(combined_running_occurrence_totals, combined_distinct_values);

    if (occurrences > max_occurrences) {
      mode = combined_values[combined_distinct_values];
      max_occurrences = occurrences;
    }

    combined_distinct_values++;
  }

  result[0] = Int32GetDatum(combined_distinct_values);
  result[1] = Float8GetDatum(unpack_value_sum_from_real_histogram(t1) + unpack_value_sum_from_real_histogram(t2));
  result[2] = mode;
  result[3] = PointerGetDatum(construct_array(combined_values, combined_distinct_values, FLOAT4OID, sizeof(float4), true, 'z'));
  result[4] = PointerGetDatum(construct_array(combined_running_occurrence_totals, combined_distinct_values, INT8OID, sizeof(int64), true, 'i'));

  result_isnull[0] = false;
  result_isnull[1] = false;
  result_isnull[2] = false;
  result_isnull[3] = false;
  result_isnull[4] = false;

  /* Form and return the tuple. */
  tuple = heap_form_tuple(tupdesc, result, result_isnull);
  PG_RETURN_DATUM(HeapTupleGetDatum(tuple));
}
