#include "fmgr.h"

typedef struct OSAPerQueryState
{
  /* Representative Aggref for this aggregate: */
  Aggref	   *aggref;
  /* Memory context containing this struct and other per-query data: */
  MemoryContext qcontext;
  /* Context for expression evaluation */
  ExprContext *econtext;
  /* Do we expect multiple final-function calls within one group? */
  bool		rescan_needed;

  /* These fields are used only when accumulating tuples: */

  /* Tuple descriptor for tuples inserted into sortstate: */
  TupleDesc	tupdesc;
  /* Tuple slot we can use for inserting/extracting tuples: */
  TupleTableSlot *tupslot;
  /* Per-sort-column sorting information */
  int			numSortCols;
  AttrNumber *sortColIdx;
  Oid		   *sortOperators;
  Oid		   *eqOperators;
  Oid		   *sortCollations;
  bool	   *sortNullsFirsts;
  /* Equality operator call info, created only if needed: */
  ExprState  *compareTuple;

  /* These fields are used only when accumulating datums: */

  /* Info about datatype of datums being sorted: */
  Oid			sortColType;
  int16		typLen;
  bool		typByVal;
  char		typAlign;
  /* Info about sort ordering: */
  Oid			sortOperator;
  Oid			eqOperator;
  Oid			sortCollation;
  bool		sortNullsFirst;
  /* Equality operator call info, created only if needed: */
  FmgrInfo	equalfn;
} OSAPerQueryState;


typedef struct OSAPerGroupState
{
  /* Link to the per-query state for this aggregate: */
  OSAPerQueryState *qstate;
  /* Memory context containing per-group data: */
  MemoryContext gcontext;
  /* Sort object we're accumulating data in: */
  Tuplesortstate *sortstate;
  /* Number of normal rows inserted into sortstate: */
  int64		number_of_rows;
  /* Have we already done tuplesort_performsort? */
  bool		sort_done;
} OSAPerGroupState;

void set_occurrences(Datum *running_occurrence_totals, int index, int occurrences);
int get_occurrences(int64 *running_occurrence_totals, int index);
int get_occurrences_from_datums(Datum *running_occurrence_totals, int index);
int unpack_distinct_values_from_histogram(HeapTupleHeader t);
int64 *unpack_running_occurrence_totals_from_histogram(HeapTupleHeader t);
ArrayType *generic_histogram_percentile_array(
    HeapTupleHeader t,
    ArrayType *percentiles_array,
    bool interpolate,
    Datum (*generic_sub)(PG_FUNCTION_ARGS),
    Datum (*float4_generic)(PG_FUNCTION_ARGS),
    Datum (*generic_mul)(PG_FUNCTION_ARGS),
    Datum (*generic_add)(PG_FUNCTION_ARGS)
);
void unpack_generic_histogram(HeapTupleHeader t, int64 **running_occurrence_totals, Datum **values, int *distinct_values);
Datum *unpack_values_from_generic_histogram(HeapTupleHeader t);
Oid unpack_value_elem_type_from_histogram(HeapTupleHeader t);
