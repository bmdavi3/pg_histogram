#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "postgres.h"
#include <catalog/pg_type.h>
#include <utils/array.h>
#include "access/htup_details.h"
#include "executor/executor.h"
#include "funcapi.h"
#include "utils/builtins.h"
#include "utils/fmgrprotos.h"
#include "utils/lsyscache.h"
#include "utils/numeric.h"

#include "histogram.h"


Datum unpack_value_sum_from_numeric_histogram(HeapTupleHeader t);
Datum unpack_value_sum_from_numeric_histogram(HeapTupleHeader t) {
  bool is_null;

  return GetAttributeByName(t, "value_sum", &is_null);
}


Datum unpack_mode_from_numeric_histogram(HeapTupleHeader t);
Datum unpack_mode_from_numeric_histogram(HeapTupleHeader t) {
  bool is_null;

  return GetAttributeByName(t, "mode", &is_null);
}


PG_FUNCTION_INFO_V1(numeric_histogram_percentile_disc_array);
Datum numeric_histogram_percentile_disc_array(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  ArrayType *percentiles_array = PG_GETARG_ARRAYTYPE_P(1);
  ArrayType *results_array;

  results_array = generic_histogram_percentile_array(t, percentiles_array, false, numeric_sub, float4_numeric, numeric_mul, numeric_add);

  PG_RETURN_ARRAYTYPE_P(results_array);
}


PG_FUNCTION_INFO_V1(numeric_histogram_percentile_cont_array);
Datum numeric_histogram_percentile_cont_array(PG_FUNCTION_ARGS) {
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  ArrayType *percentiles_array = PG_GETARG_ARRAYTYPE_P(1);
  ArrayType *results_array;

  results_array = generic_histogram_percentile_array(t, percentiles_array, true, numeric_sub, float4_numeric, numeric_mul, numeric_add);

  PG_RETURN_ARRAYTYPE_P(results_array);
}


typedef struct NumericUnnestContext {
  int64 *running_occurrence_totals;
  Datum *values;
  int pairs_idx;
} NumericUnnestContext;


PG_FUNCTION_INFO_V1(numeric_histogram_unnest);
Datum numeric_histogram_unnest(PG_FUNCTION_ARGS)
{
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  FuncCallContext     *funcctx;
  int                  call_cntr;
  int                  max_calls;
  int distinct_values;
  int64 *running_occurrence_totals;
  Datum *values;
  NumericUnnestContext *unnest_context;

  /* stuff done only on the first call of the function */
  if (SRF_IS_FIRSTCALL()) {
    MemoryContext   oldcontext;

    /* create a function context for cross-call persistence */
    funcctx = SRF_FIRSTCALL_INIT();

    /* switch to memory context appropriate for multiple function calls */
    oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

    funcctx->user_fctx = (NumericUnnestContext *)palloc0(sizeof(NumericUnnestContext));

    unnest_context = (NumericUnnestContext *)funcctx->user_fctx;

    unpack_generic_histogram(t, &running_occurrence_totals, &values, &distinct_values);

    /* Stick them on unnest_context */
    unnest_context->running_occurrence_totals = running_occurrence_totals;
    unnest_context->values = values;

    unnest_context->pairs_idx = 0;

    funcctx->max_calls = running_occurrence_totals[distinct_values - 1];  // Total number of tuples to be returned

    MemoryContextSwitchTo(oldcontext);
  }

  /* stuff done on every call of the function */
  funcctx = SRF_PERCALL_SETUP();

  call_cntr = funcctx->call_cntr;
  max_calls = funcctx->max_calls;

  unnest_context = (NumericUnnestContext *)funcctx->user_fctx;
  running_occurrence_totals = unnest_context->running_occurrence_totals;
  values = unnest_context->values;

  if (call_cntr < max_calls) {
    if (call_cntr >= running_occurrence_totals[unnest_context->pairs_idx]) {
      unnest_context->pairs_idx++;
    }
    SRF_RETURN_NEXT(funcctx, values[unnest_context->pairs_idx]);
  }
  else {
    SRF_RETURN_DONE(funcctx);
  }
}


PG_FUNCTION_INFO_V1(agg_numeric_to_histogram_final);
Datum agg_numeric_to_histogram_final(PG_FUNCTION_ARGS)
{
  OSAPerGroupState *osastate;
  Datum	val;
  bool isnull;
  Datum previous_value = DirectFunctionCall1(int4_numeric, Int32GetDatum(0));
  int pairs_index = 0;
  int occurrences = 0;
  int max_occurrences = 0;
  int i = 0;
  int num_sorted_values = 0;
  Datum *sorted_values;
  int distinct_values = 0;
  Datum value_sum = DirectFunctionCall1(int4_numeric, Int32GetDatum(0));
  Datum mode = DirectFunctionCall1(int4_numeric, Int32GetDatum(0));
  TupleDesc tupdesc;
  Datum result[5];
  bool result_isnull[5];
  HeapTuple tuple;
  Datum *values;
  Datum *running_occurrence_totals;

  /* Get the tupdesc for our call to heap_form_tuple() at the end.
   * Postgres knows the return type of our function, so this is the easiest way to get it */
  get_call_result_type(fcinfo, NULL, &tupdesc);
  BlessTupleDesc(tupdesc);

  Assert(AggCheckCallContext(fcinfo, NULL) == AGG_CONTEXT_AGGREGATE);

  /* If there were no regular rows, the result is NULL */
  if (PG_ARGISNULL(0)) {
    PG_RETURN_NULL();
  }

  osastate = (OSAPerGroupState *) PG_GETARG_POINTER(0);

  /* number_of_rows could be zero if we only saw NULL input values */
  if (osastate->number_of_rows == 0) {
    PG_RETURN_NULL();
  }

  /* Finish the sort, or rescan if we already did */
  if (!osastate->sort_done) {
    tuplesort_performsort(osastate->sortstate);
    osastate->sort_done = true;
  }
  else {
    tuplesort_rescan(osastate->sortstate);
  }
  num_sorted_values = osastate->number_of_rows;

  sorted_values = (Datum*)palloc0(sizeof(Datum) * num_sorted_values);

  /* Get the number of distinct values, so we know how big our histogram will be */
  if (tuplesort_getdatum(osastate->sortstate, true, &val, &isnull, NULL)) {
    distinct_values++;
    sorted_values[i] = val;
    previous_value = sorted_values[i];
    i++;
  }

  while(tuplesort_getdatum(osastate->sortstate, true, &val, &isnull, NULL)) {
    if (DatumGetBool(DirectFunctionCall2(numeric_ne, previous_value, val))) {
      distinct_values++;
      previous_value = val;
    }
    sorted_values[i] = val;
    i++;
  }

  /* Now that we can palloc0 our values and running_occurrence_totals */
  values = (Datum*)palloc0(sizeof(Datum) * distinct_values);
  running_occurrence_totals = (Datum*)palloc0(sizeof(Datum) * distinct_values);

  i = 0;

  /* Loop through our sorted values, and record values and running_occurrence_totals */
  if (num_sorted_values > 0) {
    values[pairs_index] = sorted_values[i];
    running_occurrence_totals[pairs_index] = Int64GetDatum(1);
    previous_value = sorted_values[i];
  }

  while(i + 1 < num_sorted_values) {
    i++;
    if (DatumGetBool(DirectFunctionCall2(numeric_eq, previous_value, sorted_values[i]))) {
      running_occurrence_totals[pairs_index] = Int64GetDatum(DatumGetInt64(running_occurrence_totals[pairs_index]) + 1);
    } else {
      pairs_index++;
      values[pairs_index] = sorted_values[i];
      running_occurrence_totals[pairs_index] = Int64GetDatum(DatumGetInt64(running_occurrence_totals[pairs_index - 1]) + 1);
      previous_value = sorted_values[i];
    }
  }

  // Compute value_sum, mode
  for (i = 0; i < distinct_values; i++) {
    if (i == 0) {
      occurrences = DatumGetInt64(running_occurrence_totals[i]);
    } else {
      occurrences = DatumGetInt64(running_occurrence_totals[i]) - DatumGetInt64(running_occurrence_totals[i - 1]);
    }

    value_sum = DirectFunctionCall2(numeric_add, value_sum, DirectFunctionCall2(numeric_mul, values[i], DirectFunctionCall1(int8_numeric, Int64GetDatum(occurrences))));

    if (occurrences > max_occurrences) {
      mode = values[i];
      max_occurrences = occurrences;
    }
  }

  result[0] = Int32GetDatum(distinct_values);
  result[1] = value_sum;
  result[2] = mode;
  result[3] = PointerGetDatum(construct_array(values, distinct_values, NUMERICOID, -1, false, 'i'));
  result[4] = PointerGetDatum(construct_array(running_occurrence_totals, distinct_values, INT8OID, sizeof(int64), true, 'i'));

  result_isnull[0] = false;
  result_isnull[1] = false;
  result_isnull[2] = false;
  result_isnull[3] = false;
  result_isnull[4] = false;

  /* Form and return the tuple. */
  tuple = heap_form_tuple(tupdesc, result, result_isnull);
  PG_RETURN_DATUM(HeapTupleGetDatum(tuple));
}


PG_FUNCTION_INFO_V1(numeric_histogram_agg);
Datum numeric_histogram_agg(PG_FUNCTION_ARGS) {
  HeapTupleHeader t1 = PG_GETARG_HEAPTUPLEHEADER(0);
  HeapTupleHeader t2 = PG_GETARG_HEAPTUPLEHEADER(1);
  int distinct_values_1;
  int distinct_values_2;
  int64 *running_occurrence_totals_1;
  int64 *running_occurrence_totals_2;
  Datum *values_1;
  Datum *values_2;

  Datum *combined_values;
  Datum *combined_running_occurrence_totals;
  int combined_distinct_values = 0;
  int max_occurrences = 0;
  int occurrences = 0;
  Datum mode = DirectFunctionCall1(int4_numeric, Int32GetDatum(0));
  int h1 = 0;
  int h2 = 0;
  Datum result[5];
  bool result_isnull[5];
  HeapTuple tuple;
  TupleDesc tupdesc;

  /* Get the tupdesc for our call to heap_form_tuple() at the end.
   * Postgres knows the return type of our function, so this is the easiest way to get it */
  get_call_result_type(fcinfo, NULL, &tupdesc);
  BlessTupleDesc(tupdesc);

  unpack_generic_histogram(t1, &running_occurrence_totals_1, &values_1, &distinct_values_1);
  unpack_generic_histogram(t2, &running_occurrence_totals_2, &values_2, &distinct_values_2);

  while (h1 < distinct_values_1 || h2 < distinct_values_2) {
    if (h1 < distinct_values_1 && // Case 1: h1 has values and...
	(h2 == distinct_values_2 || // h2 doesn't OR
	 DatumGetBool(DirectFunctionCall2(numeric_lt, values_1[h1], values_2[h2])))) { // h1 < h2
      h1++;
    } else if (h2 < distinct_values_2 && // Case 2: h2 has values and...
	       (h1 == distinct_values_1 || // h1 doesn't OR
          DatumGetBool(DirectFunctionCall2(numeric_gt, values_1[h1], values_2[h2])))) { // h2 < h1
      h2++;
    } else if (DatumGetBool(DirectFunctionCall2(numeric_eq, values_1[h1], values_2[h2]))) {
      // Case 3: Both have values, h1 == h2.  Add together, increment both
      h1++;
      h2++;
    } else {
      // Panic!
      ereport(ERROR, (errcode(ERRCODE_INTERNAL_ERROR), errmsg("Should never get here")));
    }
    combined_distinct_values++;
  }

  /* Now that we can palloc0 our values and running_occurrence_totals */
  combined_values = (Datum*)palloc0(sizeof(Datum) * combined_distinct_values);
  combined_running_occurrence_totals = (Datum*)palloc0(sizeof(Datum) * combined_distinct_values);

  h1 = 0;
  h2 = 0;
  combined_distinct_values = 0;

  while (h1 < distinct_values_1 || h2 < distinct_values_2) {
    if (h1 < distinct_values_1 && // Case 1: h1 has values and...
	(h2 == distinct_values_2 || // h2 doesn't OR
	 DatumGetBool(DirectFunctionCall2(numeric_lt, values_1[h1], values_2[h2])))) { // h1 < h2
      combined_values[combined_distinct_values] = values_1[h1];
      set_occurrences(combined_running_occurrence_totals, combined_distinct_values, get_occurrences(running_occurrence_totals_1, h1));
      h1++;

    } else if (h2 < distinct_values_2 && // Case 2: h2 has values and...
	       (h1 == distinct_values_1 || // h1 doesn't OR
          DatumGetBool(DirectFunctionCall2(numeric_gt, values_1[h1], values_2[h2])))) { // h2 < h1
      combined_values[combined_distinct_values] = values_2[h2];
      set_occurrences(combined_running_occurrence_totals, combined_distinct_values, get_occurrences(running_occurrence_totals_2, h2));
      h2++;
    } else if (DatumGetBool(DirectFunctionCall2(numeric_eq, values_1[h1], values_2[h2]))) {
      // Case 3: Both have values, h1 == h2.  Add together, increment both
      combined_values[combined_distinct_values] = values_1[h1];
      set_occurrences(combined_running_occurrence_totals, combined_distinct_values, get_occurrences(running_occurrence_totals_1, h1) + get_occurrences(running_occurrence_totals_2, h2));
      h1++;
      h2++;
    } else {
      // Panic!
      ereport(ERROR, (errcode(ERRCODE_INTERNAL_ERROR), errmsg("Should never get here")));
    }

    occurrences = get_occurrences_from_datums(combined_running_occurrence_totals, combined_distinct_values);

    if (occurrences > max_occurrences) {
      mode = combined_values[combined_distinct_values];
      max_occurrences = occurrences;
    }

    combined_distinct_values++;
  }

  result[0] = Int32GetDatum(combined_distinct_values);
  result[1] = DirectFunctionCall2(numeric_add, unpack_value_sum_from_numeric_histogram(t1), unpack_value_sum_from_numeric_histogram(t2));
  result[2] = mode;
  result[3] = PointerGetDatum(construct_array(combined_values, combined_distinct_values, NUMERICOID, -1, false, 'i'));
  result[4] = PointerGetDatum(construct_array(combined_running_occurrence_totals, combined_distinct_values, INT8OID, sizeof(int64), true, 'i'));

  result_isnull[0] = false;
  result_isnull[1] = false;
  result_isnull[2] = false;
  result_isnull[3] = false;
  result_isnull[4] = false;

  /* Form and return the tuple. */
  tuple = heap_form_tuple(tupdesc, result, result_isnull);
  PG_RETURN_DATUM(HeapTupleGetDatum(tuple));
}
