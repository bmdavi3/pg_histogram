#include <math.h>

#include "postgres.h"
#include <catalog/pg_type.h>
#include "executor/executor.h"
#include "utils/builtins.h"
#include "utils/lsyscache.h"

#include "histogram.h"


int64 *unpack_running_occurrence_totals_from_histogram(HeapTupleHeader t) {
  ArrayType *running_occurrence_totals_array_type;
  bool is_null;

  running_occurrence_totals_array_type = DatumGetArrayTypeP(GetAttributeByName(t, "running_occurrence_totals", &is_null));

  return (int64 *)ARR_DATA_PTR(running_occurrence_totals_array_type);
}


Oid unpack_value_elem_type_from_histogram(HeapTupleHeader t) {
  ArrayType *values_array_type;
  bool is_null;

  values_array_type = DatumGetArrayTypeP(GetAttributeByName(t, "values", &is_null));
  return ARR_ELEMTYPE(values_array_type);
}


int unpack_distinct_values_from_histogram(HeapTupleHeader t) {
  bool is_null;

  return DatumGetInt32(GetAttributeByName(t, "distinct_values", &is_null));
}


Datum *unpack_values_from_generic_histogram(HeapTupleHeader t) {
  ArrayType *values_array_type;
  bool is_null;
  Datum *values;
  int elem_length;
  Oid elem_type;
  int16 elem_type_width;
  bool elem_type_by_value;
  char elem_type_alignment_code;
  bool *elem_null_flags;

  values_array_type = DatumGetArrayTypeP(GetAttributeByName(t, "values", &is_null));
  elem_type =  unpack_value_elem_type_from_histogram(t);

  get_typlenbyvalalign(elem_type, &elem_type_width, &elem_type_by_value, &elem_type_alignment_code);
  deconstruct_array(values_array_type, elem_type, elem_type_width, elem_type_by_value, elem_type_alignment_code, &values, &elem_null_flags, &elem_length);

  return values;
}


void unpack_generic_histogram(HeapTupleHeader t, int64 **running_occurrence_totals, Datum **values, int *distinct_values) {

  *distinct_values = unpack_distinct_values_from_histogram(t);
  *running_occurrence_totals = unpack_running_occurrence_totals_from_histogram(t);
  *values = unpack_values_from_generic_histogram(t);
}


/*
 * Get the max running_occurrence_total value (the total occurrences)
 * Multiply percentile to get the target running_occurrence_total
 * Binary search our way to the first running_occurrence_total equals
 * or exceeds the specified fraction, return that value
 */
ArrayType *generic_histogram_percentile_array(
    HeapTupleHeader t,
    ArrayType *percentiles_array,
    bool interpolate,
    Datum (*generic_sub)(PG_FUNCTION_ARGS),
    Datum (*float4_generic)(PG_FUNCTION_ARGS),
    Datum (*generic_mul)(PG_FUNCTION_ARGS),
    Datum (*generic_add)(PG_FUNCTION_ARGS)) {
  int distinct_values;
  int64 *running_occurrence_totals;
  Datum *values;
  ArrayType *results_array;
  Datum *results;
  Datum *percentiles_content;
  Datum value_difference;
  Datum remainder_proportion;
  Oid percentiles_type;
  int16 percentiles_type_width;
  int16 return_type_width;
  int percentiles_length;
  bool percentiles_type_by_value;
  bool return_type_by_value;
  bool *percentiles_null_flags;
  char percentiles_type_alignment_code;
  char return_type_alignment_code;
  float4 *percentiles;
  float4 percentile;
  int64 total_occurrences;
  int left;
  int right;
  int pivot;
  float4 exact_target;
  float4 first_row;
  int second_row;
  float4 remainder;
  Datum remainder_datum;
  Oid values_type;

  unpack_generic_histogram(t, &running_occurrence_totals, &values, &distinct_values);

  total_occurrences = running_occurrence_totals[distinct_values - 1];

  // Determine array type
  percentiles_type = ARR_ELEMTYPE(percentiles_array);

  get_typlenbyvalalign(percentiles_type, &percentiles_type_width, &percentiles_type_by_value, &percentiles_type_alignment_code);

  // Extract the array contents (as Datum objects).
  deconstruct_array(percentiles_array, percentiles_type, percentiles_type_width, percentiles_type_by_value, percentiles_type_alignment_code,
&percentiles_content, &percentiles_null_flags, &percentiles_length);

  percentiles = palloc(sizeof(float4) * percentiles_length);

  for (int i = 0; i < percentiles_length; i++) {
    percentiles[i] = DatumGetFloat4(percentiles_content[i]);
  }

  // Holds our results until we Array-ify them at the end
  results = (Datum *)palloc(sizeof(Datum) * percentiles_length);

  for (int percentile_index = 0; percentile_index < percentiles_length; percentile_index++) {
    percentile = percentiles[percentile_index];

    if (percentile < 0 || percentile > 1) {
      elog(ERROR, "percentile value %s is not between 0 and 1", DatumGetCString(DirectFunctionCall1(float4out, Float4GetDatum(percentile))));
    }

    left = 0;
    right = distinct_values - 1;
    pivot = (left + right) / 2;
    if (interpolate) {
      exact_target = percentile * (total_occurrences - 1) + 1;
      first_row = floor(exact_target);
      second_row = ceil(exact_target);
      remainder = exact_target - first_row;
    } else {
      first_row = percentile * total_occurrences;
    }
    while (pivot > left && pivot < right) {
      if (running_occurrence_totals[pivot] < first_row) {
	left = pivot;
      } else {
	right = pivot;
      }
      pivot = (left + right) / 2;
    }

    if (running_occurrence_totals[pivot] < first_row) {
      pivot++;
    }

    if (interpolate && running_occurrence_totals[pivot] < second_row) {
      value_difference = DirectFunctionCall2(generic_sub, values[pivot + 1], values[pivot]);
      remainder_datum = DirectFunctionCall1(float4_generic,  Float4GetDatum(remainder));
      remainder_proportion = DirectFunctionCall2(generic_mul, remainder_datum, value_difference);
      results[percentile_index] = DirectFunctionCall2(generic_add, values[pivot], remainder_proportion);
    } else {
      results[percentile_index] = values[pivot];
    }
  }

  values_type =  unpack_value_elem_type_from_histogram(t);

  get_typlenbyvalalign(values_type, &return_type_width, &return_type_by_value, &return_type_alignment_code);
  results_array = construct_array(results, percentiles_length, values_type, return_type_width, return_type_by_value, return_type_alignment_code);
  return results_array;
}
