CREATE EXTENSION histogram;


CREATE TABLE foo (
    bar real
);

INSERT INTO foo (bar) VALUES
    (1), (2), (2), (3), (3), (3), (3), (4), (5), (5), (5), (5), (5), (6), (6), (6), (6), (6), (6), (6), (6), (7), (8), (8), (8), (8), (8), (8), (9), (9), (9), (10), (10), (10), (10), (11), (11), (11);

DROP TABLE IF EXISTS foo_histograms;
CREATE TABLE foo_histograms (
    bar real_histogram
);

INSERT INTO foo_histograms (bar)
SELECT
    agg_real_to_histogram() WITHIN GROUP (ORDER BY bar)
FROM
    foo;

SELECT histogram_percentile_cont(bar, .75) FROM foo_histograms;

SELECT histogram_percentile_disc(bar, .75) FROM foo_histograms;

SELECT mode(bar) FROM foo_histograms;

SELECT min(bar) FROM foo_histograms;

SELECT max(bar) FROM foo_histograms;

SELECT sum(bar) FROM foo_histograms;

SELECT avg(bar) FROM foo_histograms;

SELECT count(bar) FROM foo_histograms;

SELECT histogram_percentile_disc(bar, ARRAY[.25, .5, .75]) FROM foo_histograms;

SELECT histogram_percentile_cont(bar, ARRAY[.25, .5, .75]) FROM foo_histograms;

SELECT unnest(bar) FROM foo_histograms;

DROP TABLE IF EXISTS foo_2;
CREATE TABLE foo_2 (
    bar real
);

INSERT INTO foo_2 (bar) VALUES
    (45), (-4), (0), (5), (10), (10), (2), (3), (3), (-4);

INSERT INTO foo_histograms (bar)
SELECT
    agg_real_to_histogram() WITHIN GROUP (ORDER BY bar)
FROM
    foo_2;

SELECT histogram_agg(bar) FROM foo_histograms;

