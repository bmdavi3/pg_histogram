FROM postgres:12

RUN apt-get update \
      && apt-get install -y --no-install-recommends \
      build-essential \
      postgresql-server-dev-12

RUN mkdir -p /pg_histogram
COPY . /pg_histogram

WORKDIR /pg_histogram

# Something's weird when building the extension with llvm enabled.  Try
# again in 6 months, maybe it'll work
RUN make with_llvm=no
RUN make install with_llvm=no
